﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DropDownList.aspx.cs" Inherits="CS_Database.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>My DropDown List</title>
    <meta charset="utf-8"/>

    <!-- Bootstrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    <script src="js/app.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <div class="row col-md-offset-4 col-md-4">
            <div class="form-group">
                <div id="dvName">
                    <div class="row">
                        <label for="lblName" class="control-label">Name</label>
                    </div>
                    <div class="row">
                        <asp:Label ID="lblName" runat="server" Text="No data"></asp:Label>
                    </div>
                </div>
                <br />
                <div id="dvEmail" runat="server">
                    <div class="row">
                        <label for="ddlEmails" class="control-label">Email</label>
                    </div>
                    <div class="row">
                        <asp:DropDownList ID="ddlEmails" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div>
            </div>
    </form>
</body>
</html>

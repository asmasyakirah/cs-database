﻿<%@ Page Title="Login" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="CS_Database.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">Login</div>
        </div>
        <div class="row">
            <div class="form-group">
                <div id="dvName">
                    <div class="row">
                        <label for="lblEmail" class="control-label">Email</label>
                    </div>
                    <div class="row">
                        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div id="dvPassword">
                    <div class="row">
                        <label for="lblPassword" class="control-label">Password</label>
                    </div>
                    <div class="row">
                        <asp:TextBox ID="txtPassword" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <asp:Button runat="server" />
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </div>
        </div>
    </div>
</asp:Content>

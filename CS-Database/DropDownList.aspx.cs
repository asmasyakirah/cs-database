﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS_Database
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // First time load
            if (!Page.IsPostBack)
            {
                //Bind dropdownlist 
                bindUser();
            }
            else
            {
                getUser();
            }
        }

        protected void bindUser()
        {
            String qs = "";
            qs = qs + " SELECT  *";
            qs = qs + " FROM    MD_User";

            ddlEmails.DataSource = GetData(qs);
            ddlEmails.DataTextField = "UserEmail";
            ddlEmails.DataValueField = "UserId";
            ddlEmails.DataBind();
            ddlEmails.Items.Insert(0, new ListItem("Please select Email", ""));
        }

        protected void getUser()
        {
            //String selectedEmail = ddlEmails.SelectedValue.ToString(); // Get value (user id)
            String selectedEmail = ddlEmails.Items[ddlEmails.SelectedIndex].Text; // Get text (user email)

            String qs = "";
            qs = qs + " SELECT  *";
            qs = qs + " FROM    vwUser WHERE UserEmail = '" + selectedEmail + "'";

            DataTable dt = GetData(qs);
            if ((dt != null)&&(dt.Rows.Count>0))
            {
                DataRow row = dt.Rows[0];
                lblName.Text = row["staff_name"].ToString();
            }
            // If no user selected OR user does not exist in database
            else
            {
                lblName.Text = "No data";
            }
        }

        private static DataTable GetData(string query)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["MDConn"].ConnectionString;
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = query;
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        using (DataSet ds = new DataSet())
                        {
                            DataTable dt = new DataTable();
                            sda.Fill(dt);
                            return dt;
                        }
                    }
                }
            }
        }
    }
}
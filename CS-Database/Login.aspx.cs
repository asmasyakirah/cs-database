﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS_Database
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // First time load
            if (Page.IsPostBack)
            {
                loginUser();
            }

        }

        private void loginUser()
        {
            String userEmail = txtEmail.Text;
            String userPassword = txtPassword.Text;

            String qs = "";
            qs = qs + " SELECT  *";
            qs = qs + " FROM    vwUser";
            qs = qs + " WHERE   UserEmail = '" + userEmail + "'";

            DataTable dt = GetData(qs);
            if ((dt != null) && (dt.Rows.Count > 0))
            {
                DataRow row = dt.Rows[0];
                lblMessage.Text = row["staff_name"].ToString();
            }
            // If no user selected OR user does not exist in database
            else
            {
                lblMessage.Text = "Wrong username/password";
            }
        }

        private static DataTable GetData(string query)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["MDConn"].ConnectionString;
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = query;
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        using (DataSet ds = new DataSet())
                        {
                            DataTable dt = new DataTable();
                            sda.Fill(dt);
                            return dt;
                        }
                    }
                }
            }
        }
    }
}